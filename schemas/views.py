from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.views.generic import CreateView, ListView, DetailView, UpdateView, DeleteView
from .forms import *


class SchemaList(ListView):
    model = Schemas
    template_name = 'data_schemas.html'
    context_object_name = 'schema'
    paginate_by = 15

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Data schemas'
        return context


class ViewSchema(DetailView):
    model = Schemas
    context_object_name = 'schema_item'
    template_name = 'schema_detail.html'


class CreateSchema(CreateView):
    form_class = SchemaForm
    template_name = 'schema_create.html'
    raise_exception = True

    def schema_form(request):
        if request.method == "POST":
            form = SchemaForm(request.POST)
            if form.is_valid():
                form.save()
        else:
            form = SchemaForm()
        return render(request, 'schema_create.html', {'form': form})


class SchemaUpdateView(UpdateView):
    form_class = SchemaForm
    template_name = 'schema_update.html'
    pk_url_kwarg = 'pk'
    model = Schemas

    def form_valid(self, form):
        form.save()
        return HttpResponse("<h1>Updated</h1>")


class SchemaDeleteView(DeleteView):
    pk_url_kwarg = 'pk'
    queryset = Schemas.objects.all()
    template_name = 'schema_delete.html'

    def form_valid(self, form):
        self.object = self.get_object()
        self.object.delete()
        return HttpResponse('Delete success')
