# Generated by Django 4.1.7 on 2023-03-10 13:24

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schemas', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='schemas',
            options={'ordering': ['-created_at'], 'verbose_name': 'Schema', 'verbose_name_plural': 'Schemas'},
        ),
    ]
