from django.core import validators
from django.db import models
from django.urls import reverse


class Types(models.Model):
    type_name = models.CharField(max_length=50, unique=True)
    order = models.ManyToManyField('Order', blank=True)


class Schemas(models.Model):
    title = models.CharField(max_length=50)
    separator = models.CharField(max_length=5)
    created_at = models.DateField(auto_now_add=True)
    column_name = models.CharField(max_length=50)
    types = models.ManyToManyField(Types, blank=True)
    order = models.ManyToManyField('Order', blank=True)

    def get_absolute_url(self):
        return reverse('schema_detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Schema'
        verbose_name_plural = 'Schemas'
        ordering = ['-created_at']


class Order(models.Model):
    queue = models.PositiveIntegerField(unique=True)
