from django.contrib import admin
from .models import *


admin.site.register(Types)
admin.site.register(Schemas)
admin.site.register(Order)
