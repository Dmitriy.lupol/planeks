from django import forms
from .models import *


class SelectForManyToMany(forms.Select):

    def value_from_datadict(self, data, files, name):
        try:
            getter = data.getlist
        except AttributeError:
            getter = data.get
        return getter(name)


class SchemaForm(forms.ModelForm):
    class Meta:
        model = Schemas
        fields = ['title', 'separator', 'column_name', 'types', 'order']
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'separator': forms.TextInput(attrs={'class': 'form-control', 'rows': 5}),
            'column_name': forms.TextInput(attrs={'class': 'form-control'}),
            'types': SelectForManyToMany(),
            'order': SelectForManyToMany()
        }