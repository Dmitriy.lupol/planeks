from django.urls import path
from .views import *


urlpatterns = [
    path('data_schemas', SchemaList.as_view(), name='data_schema'),
    path('schema_detail/<int:pk>/', ViewSchema.as_view(), name='schema_detail'),
    path('schema_create', CreateSchema.as_view(), name='schema_create'),
    path('schema_update/<int:pk>', SchemaUpdateView.as_view(), name='schema_update'),
    path('schema_delete/<int:pk>', SchemaDeleteView.as_view(), name='schema_delete')
]