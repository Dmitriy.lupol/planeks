from django.shortcuts import render, redirect
from .forms import UserLoginForms
from django.contrib.auth import login, logout


def userlogin(request):
    if request.method == 'POST':
        form = UserLoginForms(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('/')
    else:
        form = UserLoginForms()
    return render(request, 'login.html', {'form': form})


def userlogout(request):
    logout(request)
    return redirect('/')
