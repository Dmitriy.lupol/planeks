from django.urls import path
from .views import *


urlpatterns = [
    path('', userlogin, name='login'),
    path('logout/', userlogout, name='logout'),
]